import * as React from "react";
import PropTypes from "prop-types";

const Hotel = ({ hotel }) => {
  let cols = [];
  for (var key in hotel) {
    if (key === "image_url") {
      cols.push(
        <td>
          <img
            src={hotel[key]}
            alt={hotel[key]}
            style={{ maxWidth: "350px" }}
          />
        </td>
      );
    } else {
      cols.push(<td>{hotel[key]}</td>);
    }
  }

  return <tr>{cols}</tr>;
};

Hotel.protoTypes = {
  hotel: PropTypes.object
};

export default Hotel;
