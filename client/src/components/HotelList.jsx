import * as React from "react";
import PropTypes from "prop-types";
import Hotel from "./Hotel";

const HotelList = ({ hotels }) => {
  return (
    <table>
      <tr>
        <th>Hotel id</th>
        <th>Hotel Name</th>
        <th>Number of reviews</th>
        <th>Address</th>
        <th>Number of stars</th>
        <th>Ammenities</th>
        <th>Image</th>
        <th>Hotel.com Price</th>
        <th>Snaptravel Price</th>
      </tr>
      {hotels.map(hotel => {
        return <Hotel hotel={hotel} />;
      })}
    </table>
  );
};

HotelList.protoTypes = {
  hotels: PropTypes.array
};

export default HotelList;
