import React, { Component } from "react";
import * as _ from "lodash";

import "./App.css";
import HotelList from "./components/HotelList";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: "",
      checkin: "",
      checkout: "",
      hotels: []
    };
    this.onChange = this.onChange.bind(this);
    this.setHotelValues = this.setHotelValues.bind(this);
  }

  getCommonHotels(snap, retail) {
    let commonHotels = [];
    let hash = {};
    for (let i = 0; i < snap.length; i++) {
      hash[snap[i].id] = snap[i];
    }
    for (let i = 0; i < retail.length; i++) {
      if (hash.hasOwnProperty(retail[i].id)) {
        console.log(
          `SNAP: ${JSON.stringify(
            hash[retail[i].id]
          )}, RETAIL: ${JSON.stringify(retail[i])}`
        );
        let hotel = _.clone(retail[i]);
        hotel.snap_price = hash[retail[i].id].price;
        commonHotels.push(hotel);
      }
    }
    console.log(commonHotels);
    return commonHotels;
  }

  setHotelValues(res) {
    let snapHotels = _.get(res, "snap.hotels");
    let retailHotels = _.get(res, "retail.hotels");

    console.log(`SNAP: ${snapHotels}, RETAIL: ${retailHotels}`);
    let commonHotels =
      snapHotels && retailHotels
        ? this.getCommonHotels(snapHotels, retailHotels)
        : [];
    this.setState({ hotels: commonHotels });
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    console.log(this.state);
    return (
      <div className="App">
        <form>
          <div>
            <label>
              City: <input name="city" onChange={this.onChange} />
            </label>
            <label>
              Check In: <input name="checkin" onChange={this.onChange} />
            </label>
            <label>
              Check Out: <input name="checkout" onChange={this.onChange} />
            </label>
          </div>
          <button
            onClick={e => {
              e.preventDefault();
              let body = {
                data: {
                  ...this.state
                }
              };
              fetch("http://localhost:4000/search", {
                method: "POST",
                body: JSON.stringify(body),
                headers: {
                  "Content-Type": "application/json"
                }
              })
                .then(res => res.json())
                .catch(error => console.error("Error:", error))
                .then(response => {
                  console.log(response);
                  this.setHotelValues(response);
                });
            }}
          >
            Submit
          </button>
        </form>

        <div>
          <HotelList hotels={this.state.hotels} />
        </div>
      </div>
    );
  }
}

export default App;
