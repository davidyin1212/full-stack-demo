#To run code:
This code contains two seperate applications once a React Client side App and one Express Serverside App

## Client
```sh
cd client

yarn install

# Start client (runs on http://localhost:3000)
yarn start
```

## Server
```sh
cd server

npm install

# Start server (runs on http://localhost:4000)
npm start

```