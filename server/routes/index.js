var express = require("express");
var { getCache, setCache } = require("../services/cache");
var callRemote = require("../services/callRemote");
var router = express.Router();

/* GET home page. */
router.post("/", async function(req, res, next) {
  let data = req.body["data"];

  let cacheKey = `${data.city}::${data.checkin}::${data.checkout}`;
  let cacheVal = getCache(cacheKey);
  if (cacheVal) {
    res.send(cacheVal);
    return;
  }

  let snapReq = callRemote(data, "snaptravel");
  let retailReq = callRemote(data, "retail");
  let requests = [snapReq, retailReq];
  let results = await Promise.all(requests);
  let response = { snap: results[0], retail: results[1] };
  // console.log(results);
  setCache(cacheKey, response);
  res.send(response);
});

module.exports = router;
