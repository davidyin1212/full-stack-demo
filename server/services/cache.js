var cache = {};

const getCache = key => {
  //cache
  return cache[key];
};

const setCache = (key, resp) => {
  cache[key] = resp;
};

module.exports = { getCache, setCache };
