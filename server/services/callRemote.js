const rp = require("request-promise-native");

const callRemote = (data, provider) => {
  let req = Object.assign({}, { ...data, provider: provider });
  let options = {
    method: "POST",
    uri: "https://experimentation.getsnaptravel.com/interview/hotels",
    body: req,
    json: true
  };

  return rp(options)
    .then(body => {
      return body;
    })
    .catch(err => {
      console.log(err);
    });
};

module.exports = callRemote;
